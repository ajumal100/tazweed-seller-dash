import React, { useState } from 'react';
import { Navbar, 
    NavbarBrand,
} from 'reactstrap';

import Colors from '../constants/Colors';
import styled from 'styled-components';

const Header = () => {
    return <div>
        <ColoredNavbar color={Colors.primary} dark expand="md">
            <NavbarBrand href="/">Tazweed Seller Dashboard</NavbarBrand>
        </ColoredNavbar>
    </div>
}

const ColoredNavbar = styled(Navbar)`
    background: ${Colors.primary}
`

export default Header;