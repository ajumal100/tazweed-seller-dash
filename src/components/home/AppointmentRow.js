import React from 'react';
import { Row, Col, ButtonGroup, Button } from 'reactstrap';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';
import moment from 'moment';

import * as authActions from '../../store/actions/auth';
import * as bookingActions from '../../store/actions/bookings';

const AppointmentRow = (props) => {
    
    const createdDate = new Date(props.booking.createdAt);
    const bookingDate = new Date(props.booking.date);
    const bookingTime = props.booking.time;
    let ampm = bookingTime.split(' ')[1];
    let hour = bookingTime.split(':')[0];
    let minute = bookingTime.split(':')[1].split(' ')[0];
    if(ampm == "PM"){
        hour = parseInt(hour) + 12;
    }
    const now = new Date();
    now.setHours(hour, minute);
    now.setMinutes(now.getMinutes() + 30);
    let toHour = now.getHours();
    if(toHour == 12 || toHour == 0){
        ampm = ampm == 'PM' ? 'AM':'PM';
        toHour = 12;
    } else if(toHour > 12){
        ampm = 'PM';
        toHour = parseInt(toHour) - 12;
    } else {
        ampm = 'AM';
    }

    const dispatch = useDispatch();

    const handleUpdateStatus = async (id, status) => {
        try{
            dispatch(authActions.showLoading());
            await dispatch(bookingActions.changeStatus(id, status));
            dispatch(authActions.hideLoading());
        } catch(err) {
            dispatch(authActions.handleErrors(err.message));
        }
    }

    return <CustomRow>
        <Col sm="1" className="d-flex flex-column align-items-center">
            <span className="date">{bookingDate.getDate().toString().padStart(2,0)}</span> {bookingDate.toDateString().split(' ')[1]}
        </Col>
        <Col sm="3" className="d-flex flex-column align-items-center justify-content-center">
            {bookingTime} - {toHour.toString().padStart(2,0)+':'+now.getMinutes().toString().padStart(2,0)+' '+ampm}
        </Col>
        <Col sm="5" className="d-flex flex-column align-items-center justify-content-center">
            <p>{props.booking.user.name} has booked an appointment for your services <span className="bolder">{moment(createdDate).fromNow()}</span></p>
        </Col>
        <Col sm="3" className="d-flex align-items-center justify-content-center">
            <Button size="sm mr-2" color="primary" onClick={()=>{handleUpdateStatus(props.booking._id, 'approved')}}>Approve</Button>
            <Button size="sm" color="danger" onClick={()=>{handleUpdateStatus(props.booking._id, 'rejected')}}>Reject</Button>
        </Col>
    </CustomRow>
};

const CustomRow = styled(Row)`
    border-bottom: 1px solid rgba(0,0,0,.125);
    padding: 10px 15px;
    margin-right: 0;
    margin-left: 0;
    .date{
        font-size: 25px;
    }
    .bolder{
        font-weight: bold;
        font-size: 14px;
        color: #666;
    }
    p{
        margin-bottom: 0;
    }
`

export default AppointmentRow;