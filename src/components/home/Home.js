import React, { useState, useEffect, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Row, Col, Card, CardText, CardHeader, CardBody, Alert} from 'reactstrap';
import { NavLink } from 'react-router-dom';

import IconBox from '../IconBox';
import styled from 'styled-components';
import AppointmentRow from './AppointmentRow';
import NextAppointmentRow from './NextAppointmentRow';
import * as sellerActions from '../../store/actions/bookings';
import * as authActions from '../../store/actions/auth';

const Home = (props) => {
    const dispatch = useDispatch();
    const sellerSlots = useSelector(state => state.auth.slots);
    const sellerId = useSelector(state => state.auth.id);
    const bookings = useSelector(state => state.booking.bookings)
    
    const pendingBookings = bookings.filter(booking => booking.status == 'pending');
    const approvedBookings = bookings.filter(booking => booking.status == 'approved');
    const rejectedBookings = bookings.filter(booking => booking.status == 'rejected');

    const loadSellerData = async () => {
        try{
            dispatch(authActions.showLoading());
            await dispatch(sellerActions.getData(sellerId));
            dispatch(authActions.hideLoading());
        } catch(err) {
            dispatch(authActions.handleErrors(err.message));
        }
    };

    useEffect(() => {
        loadSellerData();
    },[]);

    return <HomeContainer>
        <Row>
            <Col sm="4">
                <Card body className="flex-row align-items-center">
                    <IconBox icon="list-ul" color="#14A3B6"/>
                    <div className="d-flex flex-row align-items-center">
                        <CardText className="mb-2"><strong>{bookings.length}&nbsp;</strong> </CardText>
                        <CardText>Total Appointments </CardText>
                    </div>
                </Card>
            </Col>
            <Col sm="4">
                <Card body className="flex-row align-items-center">
                    <IconBox icon="question" color="#FFC309"/>
                    <div className="d-flex flex-row align-items-center">
                        <CardText className="mb-2"><strong>{pendingBookings.length}&nbsp;</strong> </CardText>
                        <CardText>Appointments Pending Approval </CardText>
                    </div>
                </Card>
            </Col>
            <Col sm="4">
                <Card body className="flex-row align-items-center">
                    <IconBox icon="thumbs-down" color="#DD3648"/>
                    <div className="d-flex flex-row align-items-center">
                        <CardText className="mb-2"><strong>{rejectedBookings.length}&nbsp;</strong> </CardText>
                        <CardText>Rejected Appointments </CardText>
                    </div>
                </Card>
            </Col>
        </Row>
        <hr/>
        <Row>
            <Col md="8">
                {
                    !sellerSlots &&
                    <Alert color="warning">
                        Your available timings are not set. Please enter your <NavLink to="/calendar">Available Timings</NavLink> to receive appointments.
                    </Alert>
                }
                <Card>
                    <CardHeader>Pending Appointments</CardHeader>
                    <CardBody className="no-padding">
                        {
                            pendingBookings.length > 0 ?
                            pendingBookings.sort((a, b) => {
                                return new Date(b.createdAt) - new Date(a.createdAt);
                            }).map(booking => {
                                return <AppointmentRow booking={booking}/>
                            }):<CardBody>No Pending bookings.</CardBody>
                        }
                    </CardBody>
                </Card>
            </Col>
            <hr/>
            <Col md="4">
                <Card>
                    <CardHeader>Upcoming Appointments</CardHeader>
                    <CardBody className="no-padding">
                        {
                            approvedBookings.length > 0 ?
                            approvedBookings.sort((a, b) => {
                                return new Date(a.date) - new Date(b.date);
                            }).map(booking => {
                                return <NextAppointmentRow booking={booking}/>
                            }):<CardBody>No Upcoming bookings.</CardBody>
                        }
                    </CardBody>
                </Card>
            </Col>
        </Row>
    </HomeContainer>
}

const HomeContainer = styled.div`
    font-size: 15px;
    strong {
        font-size: 24px;
        position: relative;
        top: 3px;
    }
    .no-padding{
        padding: 0;
    }
`

export default Home;