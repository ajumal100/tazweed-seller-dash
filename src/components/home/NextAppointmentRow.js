import React from 'react';
import { Row, Col } from 'reactstrap';
import styled from 'styled-components';
import moment from 'moment';

import IconBox from '../IconBox';
import Colors from '../../constants/Colors';

const NextAppointmentRow = (props) => {
    return <CustomRow>
        <Col sm="2" className="d-flex align-items-center">
            <IconBox icon="calendar-alt" color={Colors.primary} size="sm"/>
        </Col>
        <Col sm="10" className="upcoming-text">
        <p>Appointment scheduled on {moment(props.booking.date).format("dddd, MMMM Do YYYY")} at {props.booking.time} by <span className="bolder">{props.booking.user.name}</span></p>
        </Col>
    </CustomRow>
};

const CustomRow = styled(Row)`
    border-bottom: 1px solid rgba(0,0,0,.125);
    padding: 10px 15px;
    margin-right: 0;
    margin-left: 0;
    .upcoming-text{
        font-size: 15px;
        p{
            margin-bottom: 0;
            .bolder {
                color: #666;
                font-weight: bold;
            }
        }
    }
`

export default NextAppointmentRow;