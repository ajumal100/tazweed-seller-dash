import React from 'react';
import styled from 'styled-components';

const IconBox = (props) => {
    
    const IconContainer = styled.div`
        min-width: ${props.size == 'sm' ? "40px":"75px"};
        min-height: ${props.size == 'sm' ? "30px":"50px"};
        margin-right: 10px;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 4px;
        background-color: ${props.color};
        i{
            font-size: ${props.size == 'sm' ? "16px":"22px"};
            color: #fff;
        }
    `

    return <IconContainer>
        <i className={"fas fa-"+props.icon} />
    </IconContainer>
}



export default IconBox;