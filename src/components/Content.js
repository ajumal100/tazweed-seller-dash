import React from 'react';
import styled from 'styled-components';

const Content = (props) => {
    return <PaddedContent>
        {props.children}
    </PaddedContent>
}

export default Content;

const PaddedContent = styled.div`
    padding: 15px;
    width: 100%;
    background-color: #F5F7FB;
`