import React, { useState, useEffect } from 'react';
import { Row, 
    Col, 
    Card, 
    CardTitle, 
    CardText, 
    Label, 
    Input,
    FormGroup,
    FormText,
    Button,
    Alert
} from 'reactstrap';
import styled from 'styled-components';
import { useSelector, useDispatch } from 'react-redux';

import Colors from '../../constants/Colors';
import API from '../../constants/Api';
import * as authActions from '../../store/actions/auth';

const Settings = () => {
    const [f_services, setServices] = useState([]);
    const [f_name, setName] = useState("");
    const [f_phone, setPhone] = useState("");
    const [f_img, setImg] = useState("");
    const [f_type, setType] = useState("");
    const [f_location, setLocation] = useState("");
    const [rawImg, setRawImg] = useState("");
    const [showSaved, setShowSaved] = useState(false);

    const { id, name, phone, services, img, type, location } = useSelector(state => state.auth);
    const dispatch = useDispatch();

    useEffect(() => {
        setName(name);
        setPhone(phone);
        setImg(img);
        setServices(services);
        setType(type);
        setLocation(location);
    }, [])

    const handleServicesUpdate = (e) => {
        const text = e.target.value;
        setServices(text.split(',').map(txt => {
            return txt;
        }));
    }

    const handleImageChange = (e) => {
        const file = e.target.files[0];
        setRawImg(file);
        if(FileReader && file){
            const fileReader = new FileReader();
            fileReader.onload = () => {
                setImg(fileReader.result);
            }
            fileReader.readAsDataURL(file);
        }
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const formData  = new FormData();
        let isUpdated = false;
        const newServices = f_services.map(txt => {
            return txt.trim();
        }).filter(txt => txt != "");

        if(JSON.stringify(services) != JSON.stringify(f_services)){
            formData.append('services', newServices);
            isUpdated = true;
        }

        if(name != f_name) {
            formData.append('name', f_name);
            isUpdated = true;
        }

        if(type != f_type) {
            formData.append('type', f_type);
            isUpdated = true;
        }

        if(phone != f_phone) {
            formData.append('phone', f_phone);
            isUpdated = true;
        }

        if(location != f_location) {
            formData.append('location', f_location);
            isUpdated = true;
        }
        
        if(f_img != img){
            formData.append('img', rawImg);
            isUpdated = true;
        }

        if(!isUpdated){
            console.log("Nothing to import");
            return;
        }

        try{
            dispatch(authActions.showLoading());
            await dispatch(authActions.updateSeller(id, formData));
            setShowSaved(true);
            setTimeout(()=>{
                setShowSaved(false);
            },3000);
            dispatch(authActions.hideLoading());
        } catch(err){
            dispatch(authActions.handleErrors(err.message));
        }
        
    }

    return <SettingsContainer>
        <Row>
            <Col md="6">
                <Card body>
                    <CardTitle><strong>UPDATE YOUR DETAILS</strong></CardTitle>
                    <form onSubmit={handleSubmit}>
                        <FormGroup>
                            <Label for="phone">Seller Name</Label>
                            <Input type="text" name="name" id="name" value={f_name} placeholder="Example: Business Co. W.L.L." onChange={(e)=>{setName(e.target.value)}}/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="phone">Phone</Label>
                            <Input type="tel" name="phone" id="phone" value={f_phone} placeholder="Example: 76543210" onChange={(e)=>{setPhone(e.target.value)}}/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="type">Type of Service</Label>
                            <Input type="text" name="type" id="type" value={f_type} placeholder="Example: 'Workshop' or 'Salon'" onChange={(e)=>{setType(e.target.value)}}/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="location">Location</Label>
                            <Input type="text" name="location" id="location" value={f_location} placeholder="Example: 'Doha, Qatar'" onChange={(e)=>{setLocation(e.target.value)}}/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="exampleFile">Profile picture</Label>
                            <Input type="file" name="file" id="exampleFile" onChange={handleImageChange}/>
                            <FormText color="muted">
                                This is some placeholder block-level help text for the above input.
                                It's a bit lighter and easily wraps to a new line.
                            </FormText>
                            {
                                (f_img || img) && 
                                <img src={f_img ? (f_img.indexOf('data:') == -1 ? (API.baseUrl+'/'+f_img.replace('\\','/')): f_img) : img} 
                                    alt="profile photo" 
                                    className="img-thumbnail"/> }
                        </FormGroup>
                        <FormGroup>
                            <Label for="exampleText">Enter Services provided</Label>
                            <Input type="textarea" maxLength="200" value={f_services.join(",")} name="text" id="exampleText" onChange={handleServicesUpdate}/>
                            {   
                                f_services && 
                                <div className="tags-container">
                                    { 
                                        f_services.map((service, i) => {
                                            return <span key={i} className="tags">{service}</span>
                                        }) 
                                    }
                                </div>
                            }
                            <FormText color="muted">
                                Enter each word with comma (,) to mark as separate services. Example: 'repairs, car wash, tire change'
                            </FormText>
                        </FormGroup>
                        {showSaved && <Alert>Saved Successfully</Alert>}
                        <Button color="info" block>SAVE</Button>
                    </form>
                </Card>
            </Col>
        </Row>
    </SettingsContainer>
}

const SettingsContainer = styled.div`
    .tags-container{
        padding-top: 10px;
        .tags{
            border: 1px solid ${Colors.primary};
            border-radius: 20px;
            padding: 4px 10px;
            font-size: 12px;
            margin-right: 5px;
            color: ${Colors.primary};
            white-space: nowrap;
            line-height: 35px;
        }
    }
    textarea {
        resize: none;
    }
    .img-thumbnail{
        max-height: 300px;
    }
`

export default Settings;