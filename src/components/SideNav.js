import React, { useState } from 'react';
import { Nav, NavItem } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';

import * as authActions from '../store/actions/auth';

const SideNav = () => {
    const dispatch = useDispatch()
    const handleLogout = async (e) => {
        e.preventDefault();
        try {
            dispatch(authActions.showLoading());
            await dispatch(authActions.logout());
            dispatch(authActions.hideLoading());
        } catch(err) {
            dispatch(authActions.handleErrors(err.message));
        }
    }

    return <CustomSideNav vertical>
            <NavItem active>
                <NavLink exact to="/">
                    <i className="fas fa-home-lg"/> <span className="hidden-xs">Home</span>
                </NavLink>
            </NavItem>
            <NavItem>
                <NavLink exact to="/calendar">
                    <i className="fas fa-calendar"/> <span className="hidden-xs">Calendar</span>
                </NavLink>
            </NavItem>
            <NavItem>
                <NavLink exact to="/settings">
                    <i className="fas fa-cog"/> <span className="hidden-xs">Settings</span>
                </NavLink>
            </NavItem>
            <NavItem>
                <a href="#" onClick={handleLogout}>
                    <i className="fas fa-sign-out-alt"/> <span className="hidden-xs">Logout</span>
                </a>
            </NavItem>
        </CustomSideNav>
}

const CustomSideNav = styled(Nav)`
    width: 140px;
    background-color: #343A3F;
    @media(max-width: 767px){
        width: 60px;
        .hidden-xs{
            display: none;
        }
    }
    .nav-item {
        text-align: center;
        height: 60px;
        a{
            &.active {
                background-color: #56626d;
            }
            padding: 10px 15px;
            text-decoration: none;
            height: 100%;
            display: flex;
            justify-content: flex-start;
            color: #fff;
            font-size: 14px;
            align-items: center;
            i {
                color: #fff;
                font-size: 20px;
                margin-right: 10px;
                @media(max-width: 767px){
                    margin-right: 0;
                }
            }
        }
    }
`

export default SideNav;