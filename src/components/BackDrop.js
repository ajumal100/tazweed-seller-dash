import React from 'react';
import { Spinner } from 'reactstrap';
import styled from 'styled-components';

const BackDrop = (props) => {
    return <CustomBackDrop>
        <Spinner style={{ width: '3rem', height: '3rem' }} />{' '}
    </CustomBackDrop>
}

const CustomBackDrop = styled.div`
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: rgba(0,0,0,0.2);
    z-index: 99999;
`

export default BackDrop;