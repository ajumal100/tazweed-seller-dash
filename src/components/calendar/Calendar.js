import React, {useState, useEffect} from 'react';
import { Row, Col, Card, CardTitle, FormGroup, Input, Label, Button, Alert} from 'reactstrap';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import * as authActions from '../../store/actions/auth';

const Calendar = () => {
    const [timeSlot, setTimeSlot] = useState({});
    const [showSaved, setShowSaved] = useState(false);
    const TIME_GAP = 30;
    let slots = {};
    const week_days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const sellerID = useSelector(state => state.auth.id);
    const savedSlots = useSelector(state => state.auth.slots);

    const dispatch = useDispatch();

    const getAllHours = (timeGap) => {
        const timeArray = [];
        const time = new Date();
        time.setHours(0,0,0,0);
        do {
            let hours = time.getHours();
            let minutes = time.getMinutes();
            hours = hours % 12;
            hours = hours ? hours : 12;
            hours = hours.toString().padStart(2,0);
            const ampm = time.getHours() >= 12 ? 'PM' : 'AM';
            minutes = minutes.toString().padStart(2,0);
            timeArray.push(hours + ':' + minutes + ' ' + ampm);
            time.setMinutes(time.getMinutes() + timeGap);
        } while(time.getHours() != time.getMinutes());
        return timeArray;
    }

    const handleDayToggle = (available, day) => {
        slots = Object.assign({},timeSlot);
        if(available){
            slots[day] = [{from:'12:00 AM', to:'12:00 AM'}];
        } else {
            delete slots[day];
        }
        setTimeSlot(slots);
    }

    const handleAddHours = (dayCount) => {
        slots = Object.assign({},timeSlot);
        slots[dayCount].push({from:'12:00 AM', to:'12:00 AM'});
        setTimeSlot(slots);
    }

    const handleRemoveHour = (dayCount, hourCount) => {
        slots = Object.assign({},timeSlot);
        slots[dayCount].splice(hourCount,1);
        if(!slots[dayCount].length){
            delete slots[dayCount];
        }
        setTimeSlot(slots);
    }

    const handleSave = async () => {
        const formData = new FormData();
        formData.append('slots', JSON.stringify(timeSlot));

        try{
            dispatch(authActions.showLoading());
            await dispatch(authActions.updateSeller(sellerID, formData));
            setShowSaved(true);
            setTimeout(()=>{
                setShowSaved(false);
            },3000);
            dispatch(authActions.hideLoading());
        } catch(err){
            dispatch(authActions.handleErrors(err.message));
        }
    }

    const allHours = getAllHours(TIME_GAP);

    useEffect(() => {
        if(savedSlots != ""){
            setTimeSlot(JSON.parse(savedSlots));
        }
    }, [])
    
    return <CustomCalendarOuter>
        <Col lg="6">
            <Card body>
                <CardTitle><strong>SELECT AVAILABLE DAYS TO ADD HOURS</strong></CardTitle>
                <div className="days-selection">
                    {
                        week_days.map((day,dayCount) => {
                            return <div key={dayCount} className="day">
                                <div>
                                    <div>
                                        <FormGroup check>
                                            <Label check>
                                                <Input type="checkbox" checked={timeSlot[dayCount]&&timeSlot[dayCount].length>0} onChange={({target})=>{handleDayToggle(target.checked, dayCount)}}/>
                                                {day}
                                            </Label>
                                        </FormGroup>
                                    </div>
                                    {   
                                        timeSlot[dayCount] ?
                                        <div>
                                            {
                                                timeSlot[dayCount].map((hour, hourCount) => {
                                                    return <p key={hourCount}>{hour.from} - {hour.to}</p>
                                                })
                                            }
                                        </div> : 
                                        <div>Closed</div>
                                    }
                                </div>
                                {
                                    timeSlot[dayCount] && 
                                    <div className="collapsible">
                                        {
                                            timeSlot[dayCount].map((hour, hourCount) => {
                                                let proceed;
                                                return <div key={hourCount} className="time-selection">
                                                        <div>
                                                            <FormGroup>
                                                                <Label for={'timeSelectFrom'+dayCount+hourCount}>From</Label>
                                                                <Input type="select" 
                                                                    name="select" 
                                                                    id={'timeSelectFrom'+dayCount+hourCount} 
                                                                    value={timeSlot[dayCount][hourCount].from}
                                                                    onChange={({target})=>{
                                                                        slots = Object.assign({},timeSlot);
                                                                        slots[dayCount][hourCount].from = target.value;
                                                                        setTimeSlot(slots); 
                                                                    }}>
                                                                    {
                                                                        allHours.map(hour => {
                                                                            return <option key={hour} value={hour}>{hour}</option>
                                                                        })
                                                                    }
                                                                </Input>
                                                            </FormGroup>
                                                        </div>
                                                        <div>
                                                            —
                                                        </div>
                                                        <div>
                                                            <FormGroup>
                                                                <Label for={'timeSelectTo'+dayCount+hourCount}>To</Label>
                                                                <Input type="select" 
                                                                    name="select" 
                                                                    id={'timeSelectTo'+dayCount+hourCount} 
                                                                    value={timeSlot[dayCount][hourCount].to}
                                                                    onChange={({target})=>{
                                                                        slots = Object.assign({},timeSlot);
                                                                        slots[dayCount][hourCount].to = target.value;
                                                                        setTimeSlot(slots); 
                                                                    }}>
                                                                    {   
                                                                        proceed = false ||
                                                                        allHours.map(hour => {
                                                                            if(proceed || hour == timeSlot[dayCount][hourCount].from){
                                                                                proceed = true;
                                                                                return <option key={hour} value={hour}>{hour}</option>
                                                                            }
                                                                        })
                                                                    }
                                                                </Input>
                                                            </FormGroup>
                                                        </div>
                                                        <div>
                                                            <Button outline onClick={()=>{handleRemoveHour(dayCount, hourCount)}}><i className="fas fa-times-circle"/></Button>
                                                        </div>
                                                    </div>
                                            })
                                        }
                                        <div className="add-btn-container">
                                            <Button size="sm" outline color="primary" onClick={()=>{handleAddHours(dayCount)}}>Add more Hours</Button>
                                        </div>
                                    </div>
                                }
                            </div>
                        })
                        
                    }
                    {showSaved && <Alert>Saved Successfully</Alert>}
                    <Button color="info" block onClick={handleSave}>SAVE</Button>
                </div>
            </Card>
        </Col>
    </CustomCalendarOuter>
}

const CustomCalendarOuter = styled(Row)`
    .card-title{
        
    }
    .day{
        border-bottom: 1px solid lightgrey;
        padding: 12px 0;
        &>div:nth-child(1){
            display: flex;
            justify-content: space-between;
            align-items: center;
            input {
                margin-right: 10px;
            }
        }
        .collapsible{
            padding: 20px 25px;
            .time-selection{
                display: flex;
                justify-content: space-around;
                align-items: center;
                label{
                    font-size: 12px;
                    margin-bottom: 3px;
                }
                button{
                    border: 0;
                }
            }
            .add-btn-container{
                text-align: right;
            }
            p{
                margin-bottom: 3px;
            }
        }
    }
`

export default Calendar;