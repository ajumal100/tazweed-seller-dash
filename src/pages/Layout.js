import React from 'react';
import { useSelector } from 'react-redux';
import { BrowserRouter, Route, Switch } from "react-router-dom";

import LoginPage from './LoginPage';
import DashboardPage from './DashboardPage';
import BackDrop from '../components/BackDrop';

const Layout = () => {
    const isAuth = useSelector(state => state.auth.isAuthenticated);
    console.log("isAUTH", isAuth);
    const isLoading = useSelector(state => state.auth.isLoading);
    return <div className="App">
            {
                !isAuth ? 
                <LoginPage/>:
                <DashboardPage/>
            }
            {isLoading && <BackDrop/>} 
        </div>
}

export default Layout;