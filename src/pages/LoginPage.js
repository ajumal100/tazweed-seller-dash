import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import {
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Button,
    Row,
    Input,
    Form,
    FormGroup,
    Label,
    Progress,
    Alert,
    Spinner
} from 'reactstrap';
import { useDispatch } from 'react-redux';

import * as authActions from '../store/actions/auth';
import Errors from '../constants/Errors';

const LoginPage = (props) => {
    
    const [screenType, setScreenType] = useState("default");
    const [passwordScore, setPasswordScore] = useState(0);
    const [passColor, setPassColor] = useState("");
    const [passwordHint, setPasswordHint] = useState("");
    const [errArr, setErrArr] = useState([]);
    const [signupDisabled, setSignupDisabled] = useState(true);
    const [l_email, setL_email] = useState("");
    const [l_pwd, setL_pwd] = useState("");
    const [fullname, setFullname] = useState("");
    const [email, setEmail] = useState("");
    const [pwd, setPwd] = useState("");
    const [phone, setPhone] = useState("");
    const dispatch = useDispatch();

    useEffect(() => {
        const savedData = JSON.parse(localStorage.getItem('auth'));
        if(savedData && savedData.token){
            dispatch(authActions.autoLogin(savedData));
        }
    },[]);

    const gotoScreen = (screen) => {
        setErrArr([]);
        setScreenType(screen);
    }

    const revertToDefault = () => {
        setScreenType("default");
    }

    const calculateScore = (e) => {
        let pass = e.target.value;
        let score = 0;
        let color = "danger";
        let passwordHint = "Password Strength: Weak";
        
        if(pass.length >= 6){
            score += 25;
        }
        if(pass.match(/[a-z]/)){
            score += 25;
        }
        if(pass.match(/\d+/)){
            score += 25;
        }
        if(pass.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)){
            score += 25;
        }

        if(score > 25 && score < 75){
            passwordHint = "Please try adding a symbol (!@#) or number (0-9)"
            color = "warning";
        } else if (score == 75){
            passwordHint = "Password Strength: Strong";
            color = "success";
        } else if (score > 75){
            color = "success";
            passwordHint = "Password Strength: Very Strong";
        }
        
        setPasswordScore(score);
        setPwd(pass);
        setPassColor(color);
        setPasswordHint(passwordHint);
    }

    const onSignupFormSubmit = async (e) => {
        e.preventDefault();
        let errArray = [];
        
        if(!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(e.target.elements["email"].value)){
            errArray.push("Please enter a valid Email ID");
        }
        if(e.target.elements["fullname"].value.length < 3){
            errArray.push("Please enter your Full Name");
        }
        if(passwordScore < 70){
            errArray.push("Please enter a Strong Password");
        }
        
        if(errArray.length > 0){
            setErrArr(errArray);
            return false;
        }

        try {
            dispatch(authActions.showLoading());
            await dispatch(authActions.doSignup(fullname, email, phone, pwd));
        } catch(err) {
            dispatch(authActions.hideLoading());
            if(err.message == Errors.USER_EXISTS){
                errArray.push("There is an existing account associated with this email");
            } else {
                errArray.push(err.message);
            }
            setErrArr(errArray);
        }
    }

    const onLoginFormSubmit = async (e) => {
        e.preventDefault();
        let errArray = [];
        try{
            dispatch(authActions.showLoading());
            await dispatch(authActions.doLogin(l_email, l_pwd));
        } catch(err){
            dispatch(authActions.hideLoading());
            if(err.message == Errors.AUTH_FAIL){
                errArray.push("The password you entered is incorrect. Please try again.");
            } else {
                errArray.push(err.message);
            }
            setErrArr(errArray);
        }
    }

    return (
        <div>
            <UModal isOpen={true} onClosed={revertToDefault.bind(this)} centered={true} toggle={props.toggleModal} className={props.className}>
                <UModalHeader toggle={props.toggleModal}>
                    {screenType != "default" && <Button color="light" className="scr-backbtn" onClick={gotoScreen.bind(this, "default")}><i className="fas fa-chevron-left"/></Button>}
                    {screenType == "signup"? "Sign Up":screenType == "login" ? "Log In":null}
                </UModalHeader>
                <ModalBody>
                    {
                        screenType == "default" ?
                        <div>
                            <div className="mt-3 text-center">
                                <img src="assets/img/logo-tazweed.png"/>
                            </div>
                            <h2 className="text-center mb-5">Tazweed Seller Dashboard</h2>
                            <div className="message mt-3 mb-4">Log in or Sign up</div>
                            <Row className="px-3 mb-3">
                                <Button className="theme-color" block size="lg" onClick={gotoScreen.bind(this, "login")}>Log In</Button>
                            </Row>
                            <Row className="px-3 mb-3">
                                <Button block size="lg" color="light" onClick={gotoScreen.bind(this, "signup")}>Sign Up</Button>
                            </Row>
                        </div> : screenType == "login" ?
                        <div>
                            {errArr.length > 0 && <Alert color="danger">
                                <h6>Please fix the following issues,</h6>
                                <ul>
                                    {
                                        errArr.map((err)=>{
                                            return (
                                                <li key={err}>{err}</li>
                                            )
                                        })
                                    }
                                </ul>
                            </Alert>}
                            <Form className="signup-form" onSubmit={onLoginFormSubmit.bind(this)}>
                                <FormGroup>
                                    <Input id="l_email" name="l_email" className={(l_email.length > 0) ? "has-value":""} value={l_email} onChange={({target:{value}})=>{setL_email(value)}} type="email"/>
                                    <span className="input-border"></span>
                                    <Label for="l_email">Email</Label>
                                </FormGroup>
                                <FormGroup>
                                    <Input id="l_pwd" name="l_pwd" className={(l_pwd.length > 0) ? "has-value":""} value={l_pwd} onChange={({target:{value}})=>{setL_pwd(value)}} type="password"/>
                                    <span className="input-border"></span>
                                    <Label for="l_pwd">Password</Label>
                                </FormGroup>
                                <Button className="theme-color signup-btn mt-3 py-3" block>Login</Button>
                            </Form>
                        </div> : screenType == "signup" ?
                        <div>
                            {errArr.length > 0 && <Alert color="danger">
                                <h6>Please fix the following issues,</h6>
                                <ul>
                                    {
                                        errArr.map((err)=>{
                                            return (
                                                <li key={err}>{err}</li>
                                            )
                                        })
                                    }
                                </ul>
                            </Alert>}
                            <Form className="signup-form" onSubmit={onSignupFormSubmit.bind(this)}>
                                <FormGroup>
                                    <Input id="fullname" name="fullname" type="text" className={(fullname.length > 0) ? "has-value":""} value={fullname} onChange={({target:{value}})=>{setFullname(value)}}/>
                                    <span className="input-border"></span>
                                    <Label for="fullname">Seller Name</Label>
                                </FormGroup>
                                <FormGroup>
                                    <Input id="email" name="email" type="email" className={(email.length > 0) ? "has-value":""} value={email} onChange={({target:{value}})=>{setEmail(value)}}/>
                                    <span className="input-border"></span>
                                    <Label for="email">Email</Label>
                                </FormGroup>
                                <FormGroup>
                                    <Input id="pwd" name="pwd" type="password" className={(pwd.length > 0) ? "has-value":""} value={pwd} onChange={calculateScore.bind(this)}/>
                                    <span className="input-border"></span>
                                    <Label for="pwd">Password</Label>
                                    <Progress color={passColor} value={passwordScore}/>
                                    {
                                        pwd.length < 6 ? 
                                        <span className="password-hint">Password must be atleast 6 characters in length</span> :
                                        <span className="password-hint">{passwordHint}</span>
                                    }
                                </FormGroup>
                                <FormGroup>
                                    <Input id="phone" name="phone" placeholder="(Optional)" type="tel" className={(phone.length > 0) ? "has-value":""} value={phone} onChange={({target:{value}})=>{setPhone(value)}}/>
                                    <span className="input-border"></span>
                                    <Label for="email">Contact number</Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check className="terms-text">
                                        <i className={(signupDisabled?"fa-square":"fa-check-square")+" far checkbox-icon"}/>
                                        <Input type="checkbox" name="terms" onChange={(e)=>{ setSignupDisabled(!e.target.checked) }}/>{' '}
                                        I agree to the <strong>Terms of Service</strong>, <strong>Privacy Policy</strong> and <strong>Content Policies</strong>
                                    </Label>
                                </FormGroup>
                                <Button className="theme-color signup-btn mt-3 py-3" block disabled={signupDisabled}>Sign Up</Button>
                            </Form>
                        </div> : <div>
                            Email Verification dialog
                        </div>
                    }
                </ModalBody>
                <ModalFooter className="mt-4">
                    <p>Tazweed &reg; Seller Dashboard</p>
                </ModalFooter>
            </UModal>
            <Loading>
                <Spinner style={{ width: '3rem', height: '3rem' }} type="grow" />
            </Loading>
        </div>
    )
}

const UModalHeader = styled(ModalHeader)`
    border: 0;
    padding-bottom: 0;
`
const UModal = styled(Modal)`
    .input-border{
        display: none;
    }
    strong{
        font-weight: 600;
    }
    .message{
        text-align: center;
        font-size: 20px;
        font-weight: 600;
        line-height: 22px;
        margin-bottom: 34px;
    }
    .scr-backbtn, .scr-backbtn:active, .scr-backbtn:focus{
        background-color: #fff;
        border:0;
        position: relative;
        top: -3px;
        left: -5px;
    }
    .modal-footer{
        p{
            font-size: 9pt;
            margin-bottom: 0;
        }
    }
    .facebook-button{
        margin-bottom: 10px;
    }
    button:not(.close){
        font-size: 15px;
        i{
            float: left;
            padding-top: 5px;
        }
    }
    .text-center{
        color: #666;
    }
    .facebook-button{
        background-color: #3b5998;
        border: 1px solid #3b5998;
        border-radius: 3px;
        &:hover{
            background-color: #304d8a;
        }
    }
    .google-button.btn-secondary, .google-button.btn-secondary:focus, .google-button.btn-secondary:active{
        background-color: #fff;
        border: 1px solid #ccc;
        border-radius: 3px;
        color: #666;
        i{
            background: url(static/img/btn_google_light_focus_ios.svg);
            height: 22px;
            background-size: 3em;
            background-position: -13px -11px;
            background-repeat: no-repeat;
        }
        &:hover{
            border-color: #333;
        }
    }
    .signup-form{
        .terms-text{
            font-size: 1em;
            line-height: 17px;
        }
        .signup-btn{
            padding: 10px 0;
        }
        [type="text"],[type="email"],[type="password"]{
            padding: 20px .75rem;
        }
        input:focus {
            outline: 0;
            box-shadow: none;
        }
        .password-hint{
            color: #aaa;
        }
        .checkbox-icon{
            position: absolute;
            left: 0;
            top: 3px;
            z-index: 10;
            background: #fff;
        }
        .form-group{
            padding-top: 30px;
            position: relative;
        }
        label:not(.terms-text){
            position: absolute;
            font-weight: 600;
            top: 0;
        }
    }
    @media screen and (max-width: 767px){
        margin: 0;
        position: fixed;
        bottom: 0;
        top: 0;
        left: 0;
        right: 0;
        .modal-content{
            margin: 0;
            border-radius: 0;
            border: 0;
            height: 100%;
        }
        .password-hint{
            font-size: 14px;
        }
        .signup-form{
            .form-group{
                position: relative;
                padding: 1rem 0;
                margin: 0;
            }
            .input-border{
                position: relative;
                display: block;
                width: 100%;
            }
            .input-border:before, .input-border:after{
                transition-timing-function: cubic-bezier(.4,0,.2,1);
                transition-duration: .3s;
                position: absolute;
                bottom: 0;
                width: 0;
                height: 2px;
                content: "";
                background-color: #cb202d;
                transition-property: width,background-color;
            }
            .input-border:before{
                left: 50%;
            }
            .input-border:after{
                right: 50%;
            }
            label:not(.terms-text){
                position: absolute;
                top: 23px;
                left: 0;
                font-size: 15px;
                line-height: 19px;
                color: #89959b;
                pointer-events: none;
                transition-timing-function: cubic-bezier(.4,0,.2,1);
                transition-duration: .35s;
                transition-property: top,font-size,color;
                font-weight: normal;
            }
            [type="text"],[type="email"],[type="password"]{
                display: block;
                width: 100%;
                padding: 3px 0 8px;
                font-size: 15px;
                color: #000;
                background-color: inherit;
                border: 0;
                border-radius: 0;
                border-bottom: 2px solid #d9d9d9;
                outline: none;
                &:focus:not([disabled]):not([readonly]){
                    border-bottom-color: #cb202d;
                    &~label:not(.terms-text){
                        color: #cb202d;
                        top: 4px;
                        font-size: 12px;
                    }
                    &~.input-border:before, &~.input-border:after{
                        width: 50%;
                    }
                }
                &.has-value{
                    &~label:not(.terms-text){
                        top: 4px;
                        font-size: 12px;
                    }
                }
            }
        }
    }
`
const Loading = styled.div`
    position: fixed;
    height: 100%;
    width: 100%;
    z-index: 0;
    display: flex;
    justify-content: center;
    align-items: center;
`

export default LoginPage;