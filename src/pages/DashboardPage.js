import React from 'react'
import styled from 'styled-components'
import {
    Button,
    Row,
    Col,
    Input,
    Form,
    FormGroup,
    Label,
    Progress,
    Alert
} from 'reactstrap';
import { BrowserRouter, Route, Switch} from "react-router-dom";

import Header from '../components/Header';
import SideNav from '../components/SideNav';
import ErrorPage from '../components/ErrorPage';
import Home from '../components/home/Home';
import Calendar from '../components/calendar/Calendar';
import Settings from '../components/settings/Settings';
import Content from '../components/Content'

const DashboardPage = () => {
    return <BrowserRouter>
        <Header/>
        <Container>
            <SideNav/>
            <Content>
                <Switch >
                    <Route exact path='/' component={Home} />
                    <Route path="/calendar" component={Calendar} />
                    <Route path="/settings" component={Settings} />
                    <Route component={Home} />
                </Switch>
            </Content>
        </Container>
    </BrowserRouter>
}

const Container = styled.div`
    display: flex;
    height: 100%;
`

export default DashboardPage;