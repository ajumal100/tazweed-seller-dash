export default {
    primary: '#00A1AC',
    primaryFade: 'rgba(0,162,173,0.1)',
    accent: '#D02760',
    start: '#FB7F5B',
    header: '#EC5252'
}