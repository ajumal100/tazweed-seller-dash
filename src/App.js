import React from 'react';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import authReducer from './store/reducers/auth';
import bookingReducer from './store/reducers/bookings';
import Layout from './pages/Layout';
import styled from 'styled-components';

const rootReducer = combineReducers({
  auth: authReducer,
  booking: bookingReducer
});

const store = createStore(rootReducer, applyMiddleware(thunk));

function App() {
  return (
    <Provider store={store}>
      <GlobalLayout/>
    </Provider>
  );
}

const GlobalLayout = styled(Layout)`
  
`

export default App;
