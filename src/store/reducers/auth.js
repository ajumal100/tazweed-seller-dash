import { LOGIN, SIGNUP, SHOW_LOADING, HIDE_LOADING, AUTO_LOGIN, LOGOUT, UPDATE_SELLER } from '../actions/auth';

const initialState = {
    isAuthenticated: false,
    token: '',
    id: '',
    email: '',
    name: '',
    img: '',
    services: [],
    rating: '',
    phone: '',
    slots: '',
    type: '',
    location: '',
    isLoading: false
}

export default (state = initialState, action) => {
    console.log("Dispatched ", action.type, action);
    switch(action.type){
        case LOGIN:
            if(action.data.token){
                const saveObj = {
                    token: action.data.token,
                    id: action.data.user._id,
                    email: action.data.user.email,
                    name: action.data.user.name,
                    img: action.data.user.img,
                    services: action.data.user.services,
                    rating: action.data.user.rating,
                    phone: action.data.user.phone,
                    slots: action.data.user.slots,
                    type: action.data.user.type,
                    location: action.data.user.location
                }
                localStorage.setItem('auth', JSON.stringify(saveObj));
            } else {
                return;
            }
            return {
                ...state,
                token: action.data.token,
                id: action.data.user._id,
                email: action.data.user.email,
                name: action.data.user.name,
                img: action.data.user.img,
                services: action.data.user.services,
                rating: action.data.user.rating,
                phone: action.data.user.phone,
                slots: action.data.user.slots,
                type: action.data.user.type,
                location: action.data.user.location,
                isLoading: false,
                isAuthenticated: true
            }
        case SIGNUP:
            if(action.data.token){
                localStorage.setItem('auth', JSON.stringify({
                    token: action.data.token,
                    id: action.data.user._id,
                    email: action.data.user.email,
                    name: action.data.user.name,
                    img: action.data.user.img,
                    services: action.data.user.services,
                    rating: action.data.user.rating,
                    phone: action.data.user.phone,
                    slots: action.data.user.slots,
                    type: action.data.user.type,
                    location: action.data.user.location
                }));
            } else {
                return;
            }
            return {
                ...state,
                token: action.data.token,
                id: action.data.user._id,
                email: action.data.user.email,
                name: action.data.user.name,
                img: action.data.user.img,
                services: action.data.user.services,
                rating: action.data.user.rating,
                phone: action.data.user.phone,
                slots: action.data.user.slots,
                type: action.data.user.type,
                location: action.data.user.location,
                isAuthenticated: true,
                isLoading: false
            }
        case SHOW_LOADING:
            return {
                ...state,
                isLoading: true
            }
        case HIDE_LOADING:
            return {
                ...state,
                isLoading: false
            }
        case AUTO_LOGIN:
            return {
                ...state,
                token: action.data.token,
                id: action.data.id,
                email: action.data.email,
                name: action.data.name,
                img: action.data.img,
                services: action.data.services,
                rating: action.data.rating,
                phone: action.data.phone,
                slots: action.data.slots,
                type: action.data.type,
                location: action.data.location,
                isAuthenticated: true,
                isLoading: false
            }
        case UPDATE_SELLER:
            const saveObj = {
                token: state.token,
                id: action.data._id,
                email: state.email,
                name: action.data.name,
                img: action.data.img,
                services: action.data.services,
                rating: state.rating,
                phone: action.data.phone,
                slots: action.data.slots,
                type: action.data.type,
                location: action.data.location
            }
            localStorage.setItem('auth', JSON.stringify(saveObj));
            return {
                ...state,
                name: action.data.name,
                img: action.data.img,
                services: action.data.services,
                phone: action.data.phone,
                slots: action.data.slots,
                type: action.data.type,
                location: action.data.location
            }
        case LOGOUT:
            localStorage.removeItem('auth');
            return initialState;
        default:
            return state;
    }
}