import { GET_DATA, CHANGE_STATUS } from '../actions/bookings';

const initialState = {
    bookings: []
}

export default (state = initialState, action) => {
    console.log("Dispatched ", action.type, action);
    switch(action.type){
        case GET_DATA:
            return {
                bookings: action.data
            }
        case CHANGE_STATUS:
            const bookingID = action.data._id;
            const bookings = state.bookings.map(booking => {
                if(booking._id == bookingID){
                    console.log(action.data)
                    booking.status = action.data.status;
                }
                return booking;
            })
            console.log(bookings);
            return {
                bookings: bookings
            }
        default:
            return state
    }
}