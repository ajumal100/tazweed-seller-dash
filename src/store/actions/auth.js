import API from '../../constants/Api';
import Errors from '../../constants/Errors';

export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";
export const SIGNUP = "SIGNUP";
export const SHOW_LOADING = "SHOW_LOADING";
export const HIDE_LOADING = "HIDE_LOADING";
export const AUTO_LOGIN = "AUTO_LOGIN";
export const UPDATE_SELLER = "UPDATE_SELLER";

export const doLogin = (email, password) => {
    return async (dispatch, getState) => {
        // POST /sellers/login
        const response = await fetch(API.baseUrl + '/sellers/login', 
            {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                  email,
                  password
                })
              }
        );

        const responseData = await response.json();
        if (!response.ok) {
            throw new Error(responseData.errors.message);
        }
        dispatch({type: LOGIN, data: responseData});
    }
}

export const doSignup = (name, email, phone, password) => {
    return async (dispatch, getState) => {
        // POST /sellers/
        const response = await fetch(API.baseUrl + '/sellers/', 
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    name,
                    email,
                    phone,
                    password
                })
            });
        const responseData = await response.json();
        if (!response.ok) {
            throw new Error(responseData.errors.message);
        }
        dispatch({type: SIGNUP, data: responseData});
    }
}

export const showLoading = () => {
    return async dispatch => {
        dispatch({type: SHOW_LOADING});
    }
}

export const hideLoading = () => {
    return async dispatch => {
        dispatch({type: HIDE_LOADING});
    }
}

export const autoLogin = (data) => {
    return async dispatch => {
        dispatch({type: AUTO_LOGIN, data: data});
    }
}

export const logout = () => {
    return async dispatch => {
        dispatch({type: LOGOUT});
    }
}

export const updateSeller = (id, formData) => {
    return async (dispatch, getState) => {
        //PATCH data to ID /sellers/:id
        const response = await fetch(API.baseUrl + '/sellers/' + id, {
            method: 'PATCH',
            headers: {
                'Authorization': 'Basic ' + getState().auth.token
            },
            body: formData
        });
        const responseData = await response.json();
        if(!response.ok){
            throw new Error(responseData.errors.message);
        }
        dispatch({type: UPDATE_SELLER, data: responseData});
    }
}

export const handleErrors = (err) => {
    return async dispatch => {
        if(err == Errors.JWT_EXPIRED || err == Errors.JWT_MALFORMED){
            dispatch({type: LOGOUT});
        } else {
            alert("Network Error");
            dispatch({type: HIDE_LOADING});
            console.error("UNHANDLED Error: ", err);
        }
    }
}