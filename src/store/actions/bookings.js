import API from '../../constants/Api';

export const GET_DATA = "GET_DATA";
export const CHANGE_STATUS = "CHANGE_STATUS";

export const getData = (id) => {
    return async (dispatch, getState) => {
        //GET a seller by ID /sellers/:id
        const response = await fetch(API.baseUrl + '/bookings/seller/' + id, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + getState().auth.token
            }
        });
        const responseData = await response.json();
        if(!response.ok){
            throw new Error(responseData.errors.message);
        }
        dispatch({type: GET_DATA, data: responseData});
    }
}

export const changeStatus = (id, status) => {
    return async (dispatch, getState) => {
        //PATCH data to ID /sellers/:id
        const response = await fetch(API.baseUrl + '/bookings/' + id, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + getState().auth.token
            },
            body: JSON.stringify({
                id,
                status
            })
        });
        const responseData = await response.json();
        if(!response.ok){
            throw new Error(responseData.errors.message);
        }
        dispatch({type: CHANGE_STATUS, data: responseData});
    }
}